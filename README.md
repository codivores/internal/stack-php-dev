# Docker PHP environment for development purpose

Docker environment based on [Laradock project](https://laradock.io).

## Services

- Nginx (http://localhost / https://localhost)
- PHP FPM
- MariaDB
- Workspace: container that includes a rich set of pre-configured useful tools (git, composer, node, yarn, ...)
- Adminer (http://localhost:8080): web database management tool
- Mailhog (http://localhost:8025): web and API based SMTP testing

## Installation

Copy the env-example file and adapt the configuration in the new `.env` file (PHP version, path of the application, extensions ant tools).

```
cp env-example .env
```
